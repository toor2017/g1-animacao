function parseQueryString (queryString) {
    if(typeof(queryString) != 'string'){
      return queryString;
    }

    if(queryString[0] == '?'){
      queryString = queryString.substring(1, queryString.length);
    }

    var params = {}, queries, temp, i, l;
    queries = queryString.split('&');

    for ( i = 0, l = queries.length; i < l; i++ ) {
      temp = queries[i].split('=');
      params[temp[0]] = temp[1];
    }

    return params;
  }

  var options = parseQueryString(decodeURIComponent(window.location.search));
  if (options.imgVar) {
    document.getElementById('imgVar').src=options.imgVar;
  }
  if (options.txtVar) {
    document.getElementById('texto').innerHTML = options.txtVar;
  }